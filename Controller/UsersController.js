const UsersController = {
  userList: [
    { id: 1, username: 'Chattrawut', password: 'phoolakorn' },
    { id: 2, username: 'August', password: 'author' }
  ],
  lastId: 3,
  addUser (user) {
    delete user.passwordConfirm
    user.id = this.lastId++
    this.userList.push(user)
    return user
  },
  updateUser (user) {
    delete user.passwordConfirm
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
    return user
  },
  deleteUser (id) {
    const index = this.userList.findIndex(item => item.id === parseInt(id))
    this.userList.splice(index, 1)
    return { id: id }
  },
  getUsers () {
    return [...this.userList]
  },
  getUser (id) {
    const user = this.userList.find(item => item.id === parseInt(id))
    return user
  }
}

module.exports = UsersController
